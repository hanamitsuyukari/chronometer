package com.example.chronometer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //スタートボタンのオブジェクト取得とリスナー登録
        Button start = findViewById(R.id.button3);
        start.setOnClickListener(this);

        //ストップボタンのオブジェクト取得とリスナー登録
        Button stop = findViewById(R.id.button4);
        stop.setOnClickListener(this);

        //リセットボタンのオブジェクト取得とリスナー登録
        Button reset= findViewById(R.id.button5);
        reset.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        //クロノメーターのオブジェクト取得
        Chronometer c = findViewById(R.id.chronometer1);

        switch (v.getId()){
            case R.id.button3:
                //スタートボタン押下の場合
                c.start();
                break;
            case R.id.button4:
                //ストップボタン押下の場合
                c.stop();
                break;
            case R.id.button5:
                //リセットボタン押下の場合
                c.setBase(SystemClock.elapsedRealtime());
                break;
        }
    }
}
